﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RandomDataGenerator.Randomizers;
using RandomDataGenerator.FieldOptions;

namespace ConsoleApp7
{

    class Osoba
    {
        public int id;
        public string imie;
        public string nazwisko;

        public Osoba (int id, string imie, string nazwisko)
        {
            this.id = id;
            this.imie = imie;
            this.nazwisko = nazwisko;
        }

    }
    class Program
    {
        static void Main(string[] args)
        {
            List<int> lista = Enumerable.Range(100, 150).ToList();
            List<int> podzielnePrzez3 = lista.Where(x => x % 3 == 0).ToList();

            int elementyNaStronie = 25;
            int nrStrony = 2;
            List<int> strona = lista.Skip(elementyNaStronie * (nrStrony - 1))
                .Take(elementyNaStronie).ToList();

            double sredniaElementow = lista.Average();
            double suma = lista.Sum();

            //foreach (var item in strona)
            //{
            //    Console.WriteLine(item);
            //}
            //Console.WriteLine("");
            //Console.WriteLine("Srednia: " + sredniaElementow);
            //Console.WriteLine("");
            //Console.WriteLine("Suma: " + suma);
            //Console.ReadKey();

            //List<Osoba> osoby = Enumerable.Range(1, 50).Select(x => new Osoba()
            //{
            //    id = x,
            //    imie = x.ToString(),
            //    nazwisko = "aaaaa"

            //}).ToList();

            //foreach (var item in osoby)
            //{
            //    Console.WriteLine(item.id + " " + item.imie + " " + item.nazwisko);
            //}
            //Console.ReadKey();

            //IEnumerable<int> ids = osoby.Select(x => x.id);

            //foreach (var item in ids)
            //{
            //    Console.WriteLine(item);
            //}
            //Console.ReadKey();

            //List<string> nazwiska = osoby.Select(x => x.nazwisko).ToList();


            var intGenerator = RandomizerFactory.GetRandomizer(new FieldOptionsInteger());
            var nameGenerator = RandomizerFactory.GetRandomizer(new FieldOptionsFirstName());
            var lastNameGenerator = RandomizerFactory.GetRandomizer(new FieldOptionsLastName());

            Osoba random = new Osoba(intGenerator
                .Generate().Value, nameGenerator.Generate(), lastNameGenerator.Generate());

            //Console.WriteLine(random.id + " " + random.imie + " " + random.nazwisko);

            List<Osoba> stoOsob = Enumerable.Range(1, 101).Select(x => new Osoba(intGenerator
               .Generate().Value, nameGenerator.Generate(), lastNameGenerator.Generate()))
               .OrderBy(a => a.nazwisko).OrderBy(b => b.imie).ToList();

            foreach (var item in stoOsob)
            {
                Console.WriteLine(item.imie + " " + item.nazwisko);
            }

            Console.ReadKey();
        }




    }
}
